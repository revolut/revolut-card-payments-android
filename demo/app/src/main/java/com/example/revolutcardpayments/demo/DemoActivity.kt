package com.example.revolutcardpayments.demo

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import com.revolut.cardpayments.api.RevolutPaymentApi
import com.revolut.cardpayments.api.Result

private const val REVOLUT_CARD_PAYMENTS_REQUEST_CODE = 100

class DemoActivity : AppCompatActivity() {
    private val paymentWithEmailButton: Button by lazy { findViewById(R.id.launchPaymentWithEmail) }
    private val paymentButton: Button by lazy { findViewById(R.id.launchPayment) }
    private val orderIdText: TextInputEditText by lazy { findViewById(R.id.orderId) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)

        paymentWithEmailButton.setOnClickListener { startPayment("revolutpay@revolut.com") }
        paymentButton.setOnClickListener { startPayment() }
    }

    private fun startPayment(email: String? = null) {
        startActivityForResult(
            RevolutPaymentApi.buildCardPaymentIntent(
                this,
                orderIdText.text.toString(),
                RevolutPaymentApi.Configuration.CardPayment(email)
            ), REVOLUT_CARD_PAYMENTS_REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val result = RevolutPaymentApi.mapIntentToResult(resultCode, data)
        result?.let { handleResult(it) }
    }

    private fun handleResult(result: Result) {
        when (result) {
            Result.Authorised ->
                Toast.makeText(this@DemoActivity, "Payment authorised", Toast.LENGTH_LONG).show()
            is Result.Declined ->
                Toast.makeText(this@DemoActivity, "Payment declined", Toast.LENGTH_LONG).show()
            is Result.Error.ApiError ->
                Toast.makeText(this@DemoActivity, "Api error", Toast.LENGTH_LONG).show()
            is Result.Error.GenericError ->
                Toast.makeText(this@DemoActivity, "Generic error", Toast.LENGTH_LONG).show()
            is Result.Error.NetworkError ->
                Toast.makeText(this@DemoActivity, "Network error", Toast.LENGTH_LONG).show()
            is Result.Failed ->
                Toast.makeText(this@DemoActivity, "Payment failed", Toast.LENGTH_LONG).show()
            Result.Error.OrderNotAvailable ->
                Toast.makeText(this@DemoActivity, "OrderNotAvailable", Toast.LENGTH_LONG).show()
            Result.Error.OrderNotFound ->
                Toast.makeText(this@DemoActivity, "OrderNotFound", Toast.LENGTH_LONG).show()
            Result.Error.TimeoutError ->
                Toast.makeText(this@DemoActivity, "Timeout error: $result", Toast.LENGTH_LONG)
                    .show()
        }
    }
}
