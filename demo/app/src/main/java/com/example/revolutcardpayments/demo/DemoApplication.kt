package com.example.revolutcardpayments.demo

import android.app.Application
import com.revolut.cardpayments.api.Environment
import com.revolut.cardpayments.api.RevolutPaymentApi

class DemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        RevolutPaymentApi.init(environment = Environment.SANDBOX)
    }
}