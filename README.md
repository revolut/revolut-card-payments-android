#  **Revolut  Card  Payments  SDK  -  Android  documentation**

The Revolut Payments SDK for Android lets you accept card payments directly from your app. It is designed for easy implementation and usage. The SDK simplifies the process to initiate a card payment and interact with the Revolut backend to verify the payment status.

#####  NOTE
In order to use and accept card payment with Revolut, you need to have been [accepted  as  a  Merchant](https://www.revolut.com/business/help/merchant-accounts/getting-started/how-do-i-apply-for-a-merchant-account) in your [Revolut  Business](https://business.revolut.com/merchant) account.

​
###  Get  started  with  the  Revolut  Card  Payments  SDK  for  Android
Set up the Revolut Card Payments SDK to accept card payments directly in your app. It requires 4 steps:
1.  Install  the  SDK
2.  Get  your  merchant  API  key
3.  Create  an  order
4.  Provide  card  details  to  the  sdk  and  start  payment.

###  1.  Install  the  SDK
1. Since the SDK is hosted on mavenCentral, in order to fetch the dependency please add the following lines to your project level `build.gradle`:
```
allprojects {
    repositories {
        mavenCentral()
    }
}
```
2. Add the dependency to the module level `build.gradle`:
```
implementation 'com.revolut:cardpayments:1.0.0'
```
3. Sync your project

###  2.  Get  your  merchant  API  key
Go to your Revolut app to generate the [Merchant  API  key](https://business.revolut.com/settings/merchant-api). You need it as part of the authorisation header for each Merchant API request.

#####  NOTE
Use this key **only** for the production environment. For the [Revolut  Business  Sandbox  environment](https://sandbox-business.revolut.com/), use the [sandbox  API  key](https://sandbox-business.revolut.com/settings/merchant-api). For more information, see [Test  in  the  Sandbox  environment](https://developer.revolut.com/docs/accept-payments/tutorials/test-in-the-sandbox-environment/configuration)


###  3.  Create  an  order

When a user decides to make a purchase on your app, on the server side you need to create an order by sending a `POST` request to `https://merchant.revolut.com/api/1.0/orders`. You must include the authorization header in the request, which is in the following format:

`Bearer  [yourAPIKey]`

Where `[yourAPIKey]` is the production API key that you [generated  from  your  Merchant  account](https://business.revolut.com/settings/merchant-api).

Server side: Create an order via the Merchant API request:
```
curl -X "POST" "https://merchant.revolut.com/api/1.0/orders" \
   -H 'Authorization: Bearer [yourApiKey]' \
   -H 'Content-Type: application/json; charset=utf-8' \
   -d $'{
     "amount": 100,
     "currency": "GBP"
    }'
```


When the order is created successfully, the Merchant API returns a JSON array in the [response](https://developer.revolut.com/api-reference/merchant/#operation/createOrder) that looks like this:

```
{
 "id": "<ID>",
 "public_id": "<PUBLIC_ID>",
 "type": "PAYMENT",
 "state": "PENDING",
 "created_date": "2020-10-15T07:46:40.648108Z",
 "updated_date": "2020-10-15T07:46:40.648108Z",
 "order_amount": {
  "value": 100,
  "currency": "GBP"
 }
}

```
You should save the `public_id` which will be used to start a payment later on.

#####  NOTE
You must create a new order for each purchase. For more details check our [public  documentation](https://developer.revolut.com/docs/accept-payments).

###  4.  Start  the  payment
To capture the payment from your customer, you need to:

**a)** Invoke the `RevolutPaymentApi.buildCardPaymentIntent()` method which returns an intent for launching an activity with UI that is required for gathering user card details and confirming the payment. The parameters are the order's `public_id` and a `Configuration` structure, which includes the email, billing and shipping addresses. All of these fields are optional, but if the email is not provided then the user will be asked to input the email inside of the activity. Please note that shipping and billing addresses are optional, but you should also collect them, as it increases the likelihood of the payment to be accepted. The `Intent` returned by `RevolutPaymentApi.buildCardPaymentIntent()` should be launched via `startActivityForResult()`.

**b)** After the payment is completed the result of the payment will be received in the `onActivityResult()` in your activity/fragment from which the activity intent was launched, which will provide the status of the payment. Please note that there's a convenience method `RevolutPaymentApi.mapIntentToResult(resultCode:  Int,  resultIntent:  Intent?)` which can be used to map the resultCode and data intent into a `Result` structure.

Payment example:

```
startActivityForResult(
    RevolutPaymentApi.buildCardPaymentIntent(
        context = context,
        orderId = orderId,
        RevolutPaymentApi.Configuration.CardPayment(
            email = "useremail@gmail.com", //optional
            billingAddress = AddressParams( //optional billing address
                streetLine1 = "1 Canada Square",    //optional
                streetLine2 = "Sample street",      //optional
                city = "London",                    //optional
                region = "Greater London",          //optional
                country = "GB",
                postcode = "11111"                  //optional
            ),
            shippingAddress = null, //optional shipping address
            savePaymentMethodFor = null             //optional, might be null, `customer` or `merchant`
        )
    ), 0
)
```

You  should  also  implement  `onActivityResult()`  in  your  activity/fragment  from  which  you  launch  authentication  challenge  intent.  This  method  will  be  invoked  once  the  payment  is  completed. Please note that the result codes between `1000` and `1009` (included) are used by the sdk, so please avoid using them in order to avoid any collisions.

#  Methods  available

-  `fun  mapIntentToResult(resultCode:  Int,  resultIntent:  Intent?):  Result?`  -  allows  to  map  the  result  code  and  result-containing  intent  to  a  `Result`  structure.  It's  expected  to  be  used  in  order  to  map  the  result  passed  into  `onActivityResult()`  after  the  payment  is  finished.

- `fun buildCardPaymentIntent(context: Context, orderId: String, configuration: Configuration.CardPayment)` - allows to create an intent for launching an activity for gathering card details and handling authentication challenge.

- `fun init(environment: Environment)` - allows to switch between the sandbox and production environment. This method is not mandatory to be invoked, by default the environment is set to `Environment.PRODUCTION`.

#  Possible  results

-  ApiError(errorCode:  Int,  errorId:  String)

-  NetworkError(errorCode:  Int,  message:  String)

-  GenericError(message:  String?,  stackTrace:  String?)

-  OrderNotAvailable

-  OrderNotFound

-  TimeoutError

- Authorised

- Failed(failureReason: ErrorReason)

- Declined(declineReason: ErrorReason)

The possible values for `ErrorReason` are:

- HIGH_RISK

- UNKNOWN_CARD

- PICK_UP_CARD

- INVALID_CARD

- EXPIRED_CARD

- DO_NOT_HONOUR

- INVALID_EMAIL

- INVALID_AMOUNT

- RESTRICTED_CARD

- INSUFFICIENT_FUNDS

- REJECTED_BY_CUSTOMER

- CARDHOLDER_NAME_MISSING

- WITHDRAWAL_LIMIT_EXCEEDED

- THREE_DS_CHALLENGE_FAILED

- THREE_DS_CHALLENGE_ABANDONED

- THREE_DS_CHALLENGE_FAILED_MANUALLY

- TRANSACTION_NOT_ALLOWED_FOR_CARDHOLDER

- UNKNOWN